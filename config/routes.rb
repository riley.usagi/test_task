Rails.application.routes.draw do
  devise_for :users

  # Main page (list of all companies)
  root 'companies#index'

  # Companies
  resources :companies
  put '/companies/:id/:action', to: 'companies#upvote', as: 'companies_upvote'
  put '/companies/:id/:action', to: 'companies#downvote', as: 'companies_downvote'

  # Employees (users)
  put '/users/:id/:action', to: 'users#upvote', as: 'users_upvote'
  put '/users/:id/:action', to: 'users#downvote', as: 'users_downvote'

  # Favorites
  get '/favorites', to: 'favorites#index', as: :favorites

end
