# == Schema Information
#
# Table name: companies
#
#  id           :integer          not null, primary key
#  name         :string
#  image        :string
#  country_code :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Company < ActiveRecord::Base
  # Each company has many employees
  has_many :users

  # Validations
  validates :name, :country_code, presence: true
  validates :name, length: { minimum: 3 }

  # Likes system part
  acts_as_votable

  # Likes by current_user
  def self.likes(user)
    likes = []

    user.votes.where(votable_type: 'Company').each do |vote|
      likes << find(vote.votable_id)
    end

    likes
  end
end
