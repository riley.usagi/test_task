# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  username               :string
#  first_name             :string
#  last_name              :string
#  company_id             :integer
#  avatar                 :string
#  profession             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # One employee in one company
  belongs_to :company

  # Validations
  validates :first_name, :last_name, :email, :company_id, presence: true
  validates :first_name, length: { minimum: 2 } # => Jo Kwan Duk? o_O

  # Likes
  acts_as_votable
  acts_as_voter

  # Likes by current_user
  def self.likes(user)
    likes = []

    user.votes.where(votable_type: 'User').each do |vote|
      likes << find(vote.votable_id)
    end

    likes
  end
end
