# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  username               :string
#  first_name             :string
#  last_name              :string
#  company_id             :integer
#  avatar                 :string
#  profession             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#

class UsersController < ApplicationController

  # Like person (user) bu current_user
  def upvote
    @user = User.find(params[:id])
    respond_to do |format|
      @user.liked_by current_user
      @user.save
      format.html { redirect_to :back }
      # format.json { render json: { count: @user.votes_for.size } }
      format.js { render layout: false }
    end
  end

  # UnLike person (user) bu current_user
  def downvote
    @user = User.find(params[:id])
    respond_to do |format|
      @user.unliked_by current_user
      @user.save
      format.html { redirect_to :back }
      # format.json { render json: { count: @user.votes_for.size } }
      format.js { render layout: false }
    end
  end

end
