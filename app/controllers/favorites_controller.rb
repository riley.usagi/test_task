class FavoritesController < ApplicationController
  def index
    @favorite_companies = Company.likes(current_user)
    @favorite_users     = User.likes(current_user)
  end
end
