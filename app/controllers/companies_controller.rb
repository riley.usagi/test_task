# == Schema Information
#
# Table name: companies
#
#  id           :integer          not null, primary key
#  name         :string
#  image        :string
#  country_code :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class CompaniesController < ApplicationController
  before_action :authenticate_user!

  # Main page. List of all companies
  def index
    @companies = Company.all
  end

  # Company with its employees
  def show
    @company = Company.find(params[:id])
    @employees = @company.users
  end

  # Company create process
  def create
    company = Company.create(company_params)
    if company.save
      flash[:success] = 'Success'
      redirect_to root_path
    else
      flash[:error] = post.errors.to_a.join('. ')
      redirect_to :back
    end
  end

  # Like company bu current_user
  def upvote
    @company = Company.find(params[:id])
    respond_to do |format|
      @company.liked_by current_user
      @company.save
      format.html { redirect_to :back }
      # format.json { render json: { count: @company.votes_for.size } }
      format.js { render layout: false }
    end
  end

  # UnLike company bu current_user
  def downvote
    @company = Company.find(params[:id])
    respond_to do |format|
      @company.unliked_by current_user
      @company.save
      format.html { redirect_to :back }
      # format.json { render json: { count: @company.votes_for.size } }
      format.js { render layout: false }
    end
  end

  private

  def company_params
    params.require(:company).permit(:name, :country_code)
  end
end
