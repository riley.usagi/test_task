class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string  :username # for devise auth
      t.string  :first_name
      t.string  :last_name
      t.integer :company_id
      t.string  :avatar
      t.string  :profession

      t.timestamps null: false
    end
  end
end
