# Demo user
User.create!(username: 'demo', password: 'password', email: 'info@info.com',\
             first_name: 'Demo', last_name: 'user',\
             avatar: "http://api.randomuser.me/portraits/men/#{rand(1..90)}.jpg",\
             company_id: 1)

# 50 companies
50.times do
  company = Company.create!(name: Faker::Company.name,\
                            image: "/office/#{rand(1..17)}.jpg",\
                            country_code: Faker::Address.country_code)

  # With 2-10 persons in each
  rand(2..10).times do
    User.create!(username: Faker::Internet.user_name,\
                        email: Faker::Internet.email,\
                        password: Faker::Internet.password(8),\
                        first_name: Faker::Name.first_name,\
                        last_name: Faker::Name.last_name,\
                        avatar: "http://api.randomuser.me/portraits/#{%w(women men).sample}/#{rand(1..90)}.jpg",\
                        profession: Faker::Company.profession,\
                        company_id: company.id)
  end
end
