# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  username               :string
#  first_name             :string
#  last_name              :string
#  company_id             :integer
#  avatar                 :string
#  profession             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it 'has a valid factory' do
    expect(build(:user)).to be_valid
  end

  # I don't know is this really needed?
  context 'validates' do
    it { is_expected.to validate_presence_of :first_name }
    it { is_expected.to validate_presence_of :last_name }
    it { is_expected.to validate_presence_of :company_id }
  end

  context 'associations' do
    it { should belong_to(:company) }
  end

  it "returns a employee's full name as a string" do
    user = build_stubbed(:user,
                           first_name: 'Jane',
                           last_name: 'Smith'
                          )
    name = (user.first_name + ' ' + user.last_name)
    expect(name).to eq 'Jane Smith'
  end
end
