# == Schema Information
#
# Table name: companies
#
#  id           :integer          not null, primary key
#  name         :string
#  image        :string
#  country_code :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe Company, type: :model do
  it 'has a valid factory' do
    expect(build(:company)).to be_valid
  end

  # I don't know is this really needed?
  context 'validates' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :country_code }
  end

  context 'associations' do
    it { should have_many(:users) }
  end

  it "returns a Company name as a string" do
    company = build_stubbed(:company, name: 'Pepsico')
    expect(company.name).to eq 'Pepsico'
  end
end
