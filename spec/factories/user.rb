FactoryGirl.define do
  factory :user do
    id 1
    username Faker::Internet.user_name
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    company_id 1
    avatar "http://api.randomuser.me/portraits/men/1.jpg"
    profession Faker::Company.profession
    email Faker::Internet.email
    password Faker::Internet.password(8)
  end
end
